<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'can:teacher-access'], function () {

    Route::get('/classes', 'SchoolClassController@index')->name('classes');

    Route::get('/classes/{id}', 'SchoolClassController@edit')->name('classes_edit');

    Route::get('/subjects', 'SubjectController@index')->name('subject');
    Route::post('/subjects/add', 'SubjectController@store');
    Route::get('/subjects/add', 'SubjectController@add');
    Route::get('/subjects/edit/{id}', 'SubjectController@edit');
    Route::patch('/subjects/edit/{id}', 'SubjectController@update');

    Route::get('/grades', 'GradesController@index');
    Route::get('/grade/add', 'GradesController@add');
    Route::post('/grade/add', 'GradesController@store');
    Route::get('/grade/edit/{id}', 'GradesController@edit');
    Route::patch('/grade/edit/{id}', 'GradesController@update');

    Route::get('/students', 'StudentsController@index');
    Route::get('/student/add', 'StudentsController@add');
    Route::post('/student/add', 'StudentsController@store');
    Route::get('/student/edit/{id}', 'StudentsController@edit');
    Route::patch('/student/edit/{id}', 'StudentsController@upgrade');

    Route::get('/marks', 'MarksController@index');
    Route::get('/mark/add', 'MarksController@add');
    Route::post('/mark/add', 'MarksController@store');
    Route::get('/mark/edit/{id}', 'MarksController@edit');
    Route::patch('/mark/edit/{id}', 'MarksController@upgrade');

});