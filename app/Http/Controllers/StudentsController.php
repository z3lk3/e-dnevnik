<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Grade;
use Illuminate\Support\Facades\Session;

class StudentsController extends Controller
{
    public function index(){
        $students = Student::all();
        return view('students.index', compact('students'));
    }

    public function add(){

        $grades = Grade::all();
        return view('students.add', compact('grades'));
    }

    public function store(Request $request){

        $data = $request->all();
        $student = new Student();
        $student->fill($data);
        $student->save();

        return back();

    }

    public function edit($id){

        $grades = Grade::all();
        $student = Student::find($id);
        return view('students.edit', compact('student', 'grades'));
    }

    public function upgrade($id, Request $request)
    {

        $student = Student::findOrFail($id);



        $this->validate(request(),[

            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'date_of_birth' => 'required',
            'behavior' => 'required',
            'class_id' => 'required'
        ]);

        $data = $request->all();


        $student->fill($data);

        $student->save();
        Session::flash('flash_message', 'Student uspješno dodan!');

        return back();
    }
}
