<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index(){
        $subjects = Subject::all();
        return view('subjects.index', compact('subjects'));
    }

    public function add(){
        return view('subjects.add');
    }

    public function store(Request $reguest){
        $subject = new Subject();
        $subject->name = $reguest['name'];

    }

    public function edit($id){
        $subjects = Subject::find($id);
        return view('subjects.edit', compact('subjects'));
    }

    public function update($id, Request $request)
    {

        $subject = Subject::findOrFail($id);


        $this->validate(request(),[

            'name' => 'required'
        ]);

        $subject->fill($request->all());


        $subject->save();

        return back();
    }
}
