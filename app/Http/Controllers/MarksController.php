<?php

namespace App\Http\Controllers;

use App\Student;
use App\Subject;
use App\User;
use Illuminate\Http\Request;
use App\Mark;


class MarksController extends Controller
{
    public function index(){
        $marks = Mark::all();
        return view('marks.index', compact('marks'));
    }

    public function add(){

        $marks = Mark::all();
        $teachers = User::all()->where('role_id',1);
        $subjects = Subject::all();
        $students = Student::all();
        return view('marks.add', compact('marks','teachers', 'subjects', 'students'));
    }

    public function store(Request $request){

        $data = $request->all();
        $mark = new Mark();
        $mark->fill($data);
        $mark->save();

        return back();

    }

    public function edit($id){

        $marks = Mark::find($id);
        return view('marks.edit', compact('marks'));
    }

    public function upgrade($id, Request $request)
    {

        $mark = Mark::findOrFail($id);



        $this->validate(request(),[

            'mark' => 'required',
            'description' => 'required',
            'teacher_id' => 'required',
            'student_id' => 'required',
            'subject_id' => 'required'
        ]);

        $data = $request->all();


        $mark->fill($data);

        $mark->save();
        Session::flash('flash_message', 'Student uspješno dodan!');

        return back();
    }
}
