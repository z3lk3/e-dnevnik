<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GradesController extends Controller
{
    public function index(){
        return Grade::find(1)->subjects;
    }

    public function add(){
        return view('grades.add');
    }

    public function store(Request $request){

        $data = $request->all();
        $grade = new Grade();
        $grade->fill($data);
        $grade->save();

        return back();

    }

    public function edit($id){
        $grade = Grade::find($id);
        $subjects = Subject::all();
        return view('grades.edit', compact('grade', 'subjects'));
    }


    public function update($id, Request $request)
    {

        $grade = Grade::findOrFail($id);

        $subjects = request('subjects');


        $this->validate(request(),[

            'grade' => 'required',
            'department' => 'required',
            'classmaster_id' => 'required',
            'academic_year' => 'required'
        ]);

        $grade->fill($request->all());

        $grade->subjects()->detach();

        $grade->subjects()->attach($subjects);


        $grade->save();
        Session::flash('flash_message', 'Odjeljenje uspješno uređeno!');

        return back();
    }
}
