<?php

namespace App\Http\Controllers;

use App\SchoolClass;
use Illuminate\Http\Request;
use App\Grade;

class SchoolClassController extends Controller
{
    public function index(){
        $classes = Grade::all();
        return view('classes.index', compact('classes'));
    }

    public function edit($id){
        $class = Grade::find($id);
        $students = Grade::find($id)->students;
        return view('classes.edit', compact('class', 'students'));
    }
}
