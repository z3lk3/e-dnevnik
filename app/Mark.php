<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    protected $fillable = ['mark', 'description','teacher_id', 'subject_id', 'student_id'];

    public function subject(){
        return $this->belongTo(Subject::class);
    }

    public function student(){
        return $this->belongsTo(Student::class);
    }

    public function teacher(){
        return $this->belongsTo(User::class,'teacher_id');
    }
}
