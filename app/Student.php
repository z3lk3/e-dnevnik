<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Student extends Model
{
    protected $fillable = ['name', 'behavior','surname', 'address', 'phone', 'class_id', 'date_of_birth'];

    public function studentClass(){
        return $this->belongsTo(Grade::class,'class_id');
    }

    public function marks(){
        return $this->hasMany(Mark::class);
    }


}
