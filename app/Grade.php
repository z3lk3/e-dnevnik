<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $fillable = ['grade', 'department','classmaster_id'];

    public function subjects(){
        return $this->belongsToMany(Subject::class);
    }

    public function classmaster(){
        return $this->belongsTo(User::class);
    }
}
