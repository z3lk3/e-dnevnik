@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ url('subjects/add') }}">
        {{ csrf_field() }}

        <div class="row" style="margin: 15px;">
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Dodaj predmet</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-2 col-form-label">Predmet</label>
                            <div class="col-4">
                                <input name="name" class="form-control" type="text" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-4">
                <input class="form-control" type="submit" value="Spremi">
            </div>
        </div>
    </form>
@endsection