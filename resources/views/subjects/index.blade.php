@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-12">

            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">Predmeti</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Naziv</th>
                            <th>Upravljanje</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subjects as $subject)
                        <tr>
                            <td>{{$subject->id}}</td>
                            <td>{{$subject->name}}</td>
                            <td><a href="{{url('subjects/edit')}}/{{ $subject->id }}"><span
                                            class="label label-warning">UREDI</span></a>
                                <a href="{{url('subject/delete')}}/{{ $subject->id }}"><span
                                            class="label label-danger">OBRIŠI</span></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Naziv</th>
                            <th>Upravljanje</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
