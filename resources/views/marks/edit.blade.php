@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Uredi predmet</h3>
                </div>

                <form method="POST" action="{{url('subjects/edit')}}/{{ $subjects->id }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Razrednik</label>
                            <div class="col-4">
                                <input class="form-control" name="name" type="text" value="{{$subjects->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-4">
                                <input class="form-control" type="submit" value="Spremi">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection