@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-12">

            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">Predmeti</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Učenik</th>
                            <th>Ocjena</th>
                            <th>Opis</th>
                            <th>Profesor</th>
                            <th>Predmet</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($marks as $mark)
                        <tr>
                            <td>{{$mark->student->name .  " " . $mark->student->surname}}</td>
                            <td>{{$mark->mark}}</td>
                            <td>{{$mark->description}}</td>
                            <td>{{$mark->teacher->name . " " . $mark->teacher->surname}}</td>
                            <td>{{$mark->description}}</td>
                            <td><a href="{{url('mark/edit')}}/{{ $mark->id }}"><span
                                            class="label label-warning">UREDI</span></a>
                                <a href="{{url('mark/delete')}}/{{ $mark->id }}"><span
                                            class="label label-danger">OBRIŠI</span></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Učenik</th>
                            <th>Ocjena</th>
                            <th>Opis</th>
                            <th>Profesor</th>
                            <th>Predmet</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
