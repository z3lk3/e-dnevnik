@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dodaj razred</h3>
                </div>
                <form method="POST" action="{{ url('mark/add') }}">
                {{ csrf_field() }}

                <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-2 col-form-label">Ocjena</label>
                            <div class="col-4">
                                <input name="mark" class="form-control" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-search-input" class="col-2 col-form-label">Opis</label>
                            <div class="col-4">
                                <input name="description" class="form-control" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Profesor</label>
                            <select class="form-control" name="teacher_id">
                                @foreach($teachers  as $teacher)
                                    <option value="{{ $teacher->id }}">{{ $teacher->name . " " . $teacher->surname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Predmet</label>
                                <select class="form-control" name="subject_id">
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->name}}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Ucenik</label>
                            <select class="form-control" name="student_id">
                                @foreach($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->name . " " . $student->surname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-4">
                                <input class="form-control" type="submit" value="Spremi">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection