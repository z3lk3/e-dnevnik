@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <form method="POST" action="{{url('grade/edit')}}/{{ $grade->id }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="col-xs-6">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success" style="border-radius: 0px">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Uredi razred</h3>
                    </div>


                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-2 col-form-label">Razred</label>
                            <div class="col-4">
                                <input name="grade" class="form-control" type="text" value="{{$grade->grade}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-search-input" class="col-2 col-form-label">Odjeljenje</label>
                            <div class="col-4">
                                <input name="department" class="form-control" type="text"
                                       value="{{$grade->department}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Razrednik</label>
                            <select class="form-control" name="classmaster_id">
                                @foreach(\Illuminate\Foundation\Auth\User::all()->where('role_id',1) as $item)
                                    <option value="{{ $item->id }}">{{ $item->name . " " . $item->surname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-search-input" class="col-2 col-form-label">Godina</label>
                            <div class="col-4">
                                <input name="academic_year" class="form-control" type="text"
                                       value="{{$grade->academic_year}}">
                            </div>
                            <div class="form-group">
                                <div class="col-4">
                                    <input class="form-control" type="submit" value="Spremi">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">

                <div class="box box-primary">
                    <div class="box-header"><h3> Predmeti </h3><span class="small">Odaberite odgovarajuće predmete za razred.</span>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group">
                                @foreach ($subjects as $subject)
                                    <label>
                                        <input type="checkbox" value="{{ $subject->id }}" name="subjects[]"
                                               class="flat-red" {{ in_array($subject->id, $grade->subjects->pluck('id')->toArray()) ? 'checked':'' }}>
                                        <span class="label label-warning"
                                              style="padding-top: 5px"> {{ $subject->name}}</span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection