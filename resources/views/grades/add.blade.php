@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dodaj razred</h3>
                </div>
                <form method="POST" action="{{ url('grade/add') }}">
                {{ csrf_field() }}

                <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-2 col-form-label">Razred</label>
                            <div class="col-4">
                                <input name="grade" class="form-control" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-search-input" class="col-2 col-form-label">Odjeljenje</label>
                            <div class="col-4">
                                <input name="department" class="form-control" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Razrednik</label>
                            <div class="col-4">
                                <input name="classmaster_id" class="form-control" type="text" value="">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-4">
                                <input class="form-control" type="submit" value="Spremi">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Razred</th>
                <th>Razrednik</th>
            </tr>
            </thead>
            <tbody>
            @foreach($grades as $grade)
                <tr>
                    <td>{{$grade->grade}}.{{$grade->department}}</td>
                    <td>{{$grade->classmaster_id}}</td>
                    <td><a href="{{url('grades')}}/{{ $grade->id }}"><span
                                    class="label label-warning">UREDI</span></a>
                        <a href="{{url('grade/delete')}}/{{ $grade->id }}"><span
                                    class="label label-danger">OBRIŠI</span></a></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Razred</th>
                <th>Razrednik</th>
            </tr>
            </tfoot>
        </table>
    </div>



@endsection