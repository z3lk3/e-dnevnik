@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-4">
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Uredi studenta</h3>
                </div>

                <form method="POST" action="{{url('student/edit')}}/{{ $student->id }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Ime</label>
                            <div class="col-4">
                                <input class="form-control" name="name" type="text" value="{{$student->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Prezime</label>
                            <div class="col-4">
                                <input class="form-control" name="surname" type="text" value="{{$student->surname}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Adresa</label>
                            <div class="col-4">
                                <input class="form-control" name="address" type="text" value="{{$student->address}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Telefon</label>
                            <div class="col-4">
                                <input class="form-control" name="phone" type="text" value="{{$student->phone}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Datum rođenja</label>
                            <div class="col-4">
                                <input class="form-control" name="date_of_birth" type="text" value="{{$student->date_of_birth}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="example-url-input" class="col-2 col-form-label">Vladanje</label>
                            <div class="col-4">
                                <input class="form-control" name="behavior" type="text" value="{{$student->behavior}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Razred</label>
                            <select class="form-control" name="class_id">
                                @foreach(\App\Grade::all() as $grade)
                                    <option value="{{ $grade->id }}">{{ $grade->grade . "." . $grade->department }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="col-4">
                                <input class="form-control" type="submit" value="Spremi">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection