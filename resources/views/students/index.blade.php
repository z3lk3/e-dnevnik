@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-12">

            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">Učenici</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Redni broj</th>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Adresa</th>
                            <th>Telefon</th>
                            <th>Datum rođenja</th>
                            <th>Vladanje</th>
                            <th>Razred</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                        <tr>
                            <td>{{$student->id}}</td>
                            <td>{{$student->name}}</td>
                            <td>{{$student->surname}}</td>
                            <td>{{$student->address}}</td>
                            <td>{{$student->phone}}</td>
                            <td>{{$student->date_of_birth}}</td>
                            <td>{{$student->behavior}}</td>
                            <td>{{$student->studentClass->grade . "." . $student->studentClass->department  }}</td>
                            <td><a href="{{url('student/edit')}}/{{ $student->id }}"><span
                                            class="label label-warning">UREDI</span></a>
                                <a href="{{url('subject/delete')}}/{{ $student->id }}"><span
                                            class="label label-danger">OBRIŠI</span></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Redni broj</th>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Adresa</th>
                            <th>Telefon</th>
                            <th>Datum rođenja</th>
                            <th>Vladanje</th>
                            <th>Razred</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
