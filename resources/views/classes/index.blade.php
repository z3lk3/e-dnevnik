@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-12">

            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">Pregled odjeljenja</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Razred</th>
                            <th>Akademska godina</th>
                            <th>Razrednik</th>
                            <th>Upravljanje</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($classes as $class)
                        <tr>
                            <td>{{$class->id}}</td>
                            <td>{{$class->grade}}.{{$class->department}}
                            </td>
                            <td>{{$class->academic_year}}</td>
                            <td>{{ $class->classmaster->name ." ". $class->classmaster->surname }}</td>
                            <td><a href="{{url('classes')}}/{{ $class->id }}"><span
                                            class="label label-warning">UREDI</span></a>
                                <a href="{{url('classes/delete')}}/{{ $class->id }}"><span
                                            class="label label-danger">OBRIŠI</span></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Razred</th>
                            <th>Akademska godina</th>
                            <th>Razrednik</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection
