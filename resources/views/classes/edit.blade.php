@extends('layouts.app')

@section('content')
    <div class="row" style="margin: 15px;">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Informacije o razredu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-2 col-form-label">Razred</label>
                        <div class="col-4">
                            <input class="form-control" type="text" value="{{$class->grade}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-search-input" class="col-2 col-form-label">Odjeljenje</label>
                        <div class="col-4">
                            <input class="form-control" type="search" value="{{$class->department}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-email-input" class="col-2 col-form-label">Godina</label>
                        <div class="col-4">
                            <input class="form-control" type="email" value="{{$class->academic_year_id}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-url-input" class="col-2 col-form-label">Razrednik</label>
                        <div class="col-4">
                            <input class="form-control" type="url" value="{{$class->classmaster_id}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Učenici</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Ime i prezime</th>
                            <th>Adresa</th>
                            <th>Datum rođenja</th>
                            <th>Upravljanje</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td>{{$student->id}}</td>
                                <td>{{$student->name}} {{$student->surname}}
                                </td>
                                <td>{{$student->address}}</td>
                                <td>{{$student->date_of_birth}}</td>
                                <td><a href="{{url('student/edit')}}/{{ $student->id }}"><span
                                                class="label label-warning">UREDI</span></a>
                                    <a href="{{url('student/delete')}}/{{ $student->id }}"><span
                                                class="label label-danger">OBRIŠI</span></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Ime i prezime</th>
                            <th>Adresa</th>
                            <th>Datum rođenja</th>
                            <th>Razrednik</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection